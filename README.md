# widget_sliverappbar

Flutter viene con un conjunto de potentes widgets en el cual se encuentra uno de ellos que es SliverAppBar el cual sirve para proporcionar una barra de aplicaciones desplazable o pegable en este ejemplo se realizaron los dos. Básicamente este widget nos brinda los medios para crear una barra de aplicaciones que puede cambiar de apariencia, mezclarse con el fondo o incluso como en este ejemplo desaparecer a medida que nos desplazamos. 
SliverAppBa generalmente se toma como un widget secundario para proporcionar el poder de interactuar con el desplazamiento, su uso es adecuado para agregarle algún gesto a su aplicación que este realizando, más que nada para añadirle una retroalimentación visual al usuario mientras toca o se desplaza por la pantalla. La implementación de este widget me parece muy interesante para darle un toque más a las aplicaciones mostrando una imagen o fondo en la parte superior de la pantalla, ocupando un espacio fijo, que posteriormente al desplazarse hacia arriba. El contenido cambia y se convierte en una barra de navegación predeterminada.  
<div align="center">
<img src= "images/SliverAppBar.jpg" width="900px"/>
</div>
