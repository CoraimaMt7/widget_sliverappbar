import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  //Implementar contenedor para devolver varia información
  Widget resto(String menu, Color color, String image, Icon icon) {
    return Container(
      color: color,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          /* Image.asset(
            image,
            height: 50,
            width: 50,
          ), */
          CircleAvatar(
            radius: 50,
            backgroundImage: AssetImage(image),
          ),
          Text(
            menu,
            style: TextStyle(color: Colors.white, fontSize: 40),
          ),
          Icon(
            Icons.keyboard_arrow_down,
            color: Colors.white,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          'Contactos',
          style: TextStyle(color: Colors.black),
        ),
        actions: <Widget>[
          Icon(
            Icons.app_registration_sharp,
            color: Colors.black,
          )
        ],
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Text(
              'Registros',
              style: TextStyle(color: Colors.white, fontSize: 15), //
            ),
            //centerTitle: true,
            pinned: true,
            floating: false,
            expandedHeight: 240,
            backgroundColor: Colors.transparent,
            flexibleSpace: FlexibleSpaceBar(
              background: Image.asset('images/0.jpg', fit: BoxFit.cover),
            ),
            //acciones
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.verified_user),
                color: Colors.white,
                onPressed: null,
              )
            ],
          ),
          SliverFixedExtentList(
            itemExtent: 150,
            delegate: SliverChildListDelegate([
              resto('Coraima', Colors.pinkAccent, 'images/1.jpg',
                  Icon(Icons.verified_user)),
              resto('Daniela', Colors.lightGreen, 'images/2.jpg',
                  Icon(Icons.verified_user)),
              resto('Gregory', Colors.lightBlue, 'images/3.jpg',
                  Icon(Icons.verified_user)),
              resto('Valeria', Colors.orangeAccent, 'images/4.jpg',
                  Icon(Icons.verified_user)),
              resto('Barbara', Colors.purpleAccent, 'images/5.jpg',
                  Icon(Icons.verified_user)),
              resto('Julieta', Colors.limeAccent, 'images/6.jpg',
                  Icon(Icons.edit)),
              resto('Nicolas', Colors.blue, 'images/7.png',
                  Icon(Icons.verified_user)),
            ]),
          )
        ],
      ),
    );
  }
}
/* 
class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

        //
        body: CustomScrollView(
      slivers: [
        SliverAppBar(
          pinned: true,
          title: Text(
            'Sliver appbar tutorial',
          ),
          centerTitle: true,
          expandedHeight: 200,
          backgroundColor: Colors.orange,
        ),

        //
        SliverList(
            delegate: SliverChildBuilderDelegate(
          (context, index) {
            return Text('Hello world');
          },
          childCount: 100,
        )),
        // SliverFillRemaining(
        //   child: Center(
        //     child: Text(
        //       'Hello world',
        //     ),
        //   ),
        // )
      ],
    ));
  }
}
 */