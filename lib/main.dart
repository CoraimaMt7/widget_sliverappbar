//paquete para Random
//import 'dart:math';

import 'package:flutter/material.dart';
import 'home_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // Este widget raíz.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, //Eliminar banner
      title: 'Material App',
      theme: ThemeData(primaryColor: Colors.orange),
      home: HomeScreen(),
    );
  }
}
/* 
class MiPagina extends StatelessWidget {
  //traer propiedad para seleccion aleatoria
  final rnd = new Random();
  //Lista de colores
  final List<Color> colores = [
    Colors.red,
    Colors.redAccent,
    Colors.pink,
    Colors.purple,
    Colors.blue,
    Colors.blueAccent,
    Colors.blueGrey,
    Colors.grey,
    Colors.amber,
    Colors.yellow,
    Colors.yellowAccent,
    Colors.green,
    Colors.lightGreen,
    Colors.black12,
    Colors.black45,
    Colors.black,
  ];
  @override
  Widget build(BuildContext context) {
    //1. Construir una lista comun
    //Lista de colores a widgets que tengan alguna forma
    //Crear propiedad

    final List<Widget> items = List.generate(
        10,
        (i) => Container(
              //Contiene las siguientes propiedades
              width: double.infinity,
              height: 150, //px
              color: colores[rnd.nextInt(this.colores.length)],
            ));
    return Scaffold(
        //Mostrar la lista
        body: ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        //Retorna el widget colleccion > items
        return items[index];
      },
    ));
    
    
  }
}
 */